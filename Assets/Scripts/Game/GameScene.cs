﻿using AppodealAds.Unity.Api;
using Assets.Scripts.Camera;
using Assets.Scripts.Class;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.Sound;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class GameScene : MonoBehaviour
    {

        private void Awake()
        {
            CameraManager.Instance.Init();
            ADManager.Instance.IsLoaded(Appodeal.BANNER_BOTTOM);
            ADManager.Instance.Show(Appodeal.BANNER_BOTTOM);
        }

        
    }
}
