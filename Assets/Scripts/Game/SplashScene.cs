﻿using System.Collections;
using AppodealAds.Unity.Api;
using Assets.Scripts.Sound;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class SplashScene : MonoBehaviour
    {
        // Use this for initialization
        private void Start()
        {
            PlayerPrefs.SetInt(ConstLevel.AppState, 0);
            PlayerPrefs.Save();

            ADManager.Instance.Init();
            AudioManager.Instance.Initialize();
//            AudioManager.Instance.PlaySoundUi(UIAudioClipTypes.Spalsh);
           
            StartCoroutine(StartMenuScene());
        }

        private IEnumerator StartMenuScene()
        {
            yield return new WaitForSeconds(3.0f);
            Application.LoadLevel("Game_Scene");
        }



  
    }
}