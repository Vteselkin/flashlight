﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Extensions;
using UnityEngine;

namespace Assets.Scripts.Class {

    public abstract class ContainerTouch : MonoBehaviour
    {


        /// <summary>
        ///     Command dictionary for each simple UI element.
        /// </summary>
        protected Dictionary<String, ICommandMap> Commands;

        /// <summary>
        ///     Type of thee UI element.
        /// </summary>
        public UIElementType Type { get; set; }

        /// <summary>
        ///     Top UI layer value.
        /// </summary>
        public Int32 LastDepthValue { get; protected set; }

        #region Constructors

        protected ContainerTouch()
        {
            Commands = new Dictionary<string, ICommandMap>();
        }

        #endregion

        #region ##### PRIVATE METHODS #####

        /// <summary>
        ///     Subscribe all ui element to click events.
        /// </summary>
        private void Subscribe()
        {
            // if there's clickable element add event raiser component to it.
            foreach (
                var child in
                    gameObject.GetComponentsInChildren<Transform>()
                        .Where(child => child.GetComponent<BoxCollider>() != null))
                child.gameObject.AddComponent<UIEventRaiser>();


            // subscribe each object if it has Event Raiser component.
            foreach (var child in gameObject.GetComponentsInChildren<UIEventRaiser>())
                child.Click += EventWorker;
        }

        #endregion

        #region ##### PROTECTED METHODS #####

        /// <summary>
        ///     Invokes the click handler from Actions dictionary.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EventWorker(object sender, UIEventArgs e)
        {
            try
            {
                Commands[e.Info].Execute();
            }
            catch (KeyNotFoundException)
            {
#if UNITY_EDITOR
                Debug.LogWarning("There is no click handler in " + Type + " e.info is " + e.Info +
                                 "No command was executed");
#endif
            }
        }


        /// <summary>
        ///     Additional init method for init needed singletones,
        ///     or if need some init before subscribe.
        /// </summary>
        protected virtual void PreInit()
        {
        }

        /// <summary>
        ///     Inits all click handlers.
        /// </summary>
        protected abstract void InitUIHandlers();

        #endregion

        #region ##### UNITY REFLECTION CALLBACKS #####

        /// <summary>
        ///     Unity's awake reflection method.
        /// </summary>
        protected virtual void Awake()
        {
            PreInit();
            InitUIHandlers();
        }

        /// <summary>
        ///     Unity's start reflection method.
        /// </summary>
        protected virtual void Start()
        {
            Subscribe();
        }

        protected virtual void Update()
        {
        }

        /// <summary>
        ///     Unity destroy callback.
        /// </summary>
        protected virtual void OnDestroy()
        {
            Commands = null;

            foreach (UIEventRaiser child in gameObject.GetComponentsInChildren<UIEventRaiser>())
            {
                child.Click -= EventWorker;
            }
        }

        #endregion

        #region ##### PUBLIC METHODS #####

        /// <summary>
        ///     Enables all children colliders.
        /// </summary>
        public virtual void EnableInput()
        {
#if UNITY_EDITOR
            Debug.Log("Enabling input in " + Type);
#endif
            gameObject.SetColliderStateInAllChildren(true);
        }

        /// <summary>
        ///     Disabled all children colliders.
        /// </summary>
        public virtual void DisableInput()
        {
#if UNITY_EDITOR

            Debug.Log("Disabling input in " + Type);
            gameObject.SetColliderStateInAllChildren(false);
#endif
        }

        #endregion
    }
}
