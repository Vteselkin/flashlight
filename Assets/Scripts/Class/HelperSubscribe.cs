﻿using Assets.Scripts.Map.Interface;

using UnityEngine;

namespace Assets.Scripts.Map.Class
{
    public abstract class HelperSubscribe : MonoBehaviour, ISubscribe
    {

        void Awake()
        {
            Init();
            //Debug.Log("Subscribe to all events");
            Subscribe();
        }


        void OnDestroy()
        {
            //Debug.Log("Unsubscribe from all events");

                Unsubscribe();
        }
        public abstract void Init();
        public abstract void Subscribe();
        public abstract void Unsubscribe();
    }
}