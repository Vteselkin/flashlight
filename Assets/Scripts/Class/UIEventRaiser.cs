﻿#region Usings

using System;
using UnityEngine;

#endregion

public class UIEventRaiser : MonoBehaviour
{

    private void OnClick()
    {
        this.OnClick(this.gameObject.name);
    }

    private void OnMouseDown()
    {
        this.OnClick(this.gameObject.name);

    }
    public event EventHandler<UIEventArgs> Click;

    private void OnClick(string info)
    {
        EventHandler<UIEventArgs> handler = this.Click;
        if (handler != null)
        {
            handler(this, new UIEventArgs(info));
        }
    }

}

public class UIEventArgs : EventArgs
{

    public UIEventArgs(string info)
    {
        this.Info = info;
    }

    public String Info { get; private set; }

}