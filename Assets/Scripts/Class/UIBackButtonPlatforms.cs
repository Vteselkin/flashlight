﻿using UnityEngine;

public class UIBackButtonPlatforms : MonoBehaviour
{
    // Update is called once per frame
    private void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
            BackAndroid();
    }

    private void BackAndroid()
    {
        if (!Input.GetKey(KeyCode.Escape)) return;
        Application.Quit();
    }
}