﻿using System;

namespace Assets.Scripts.Map.Classes
{
    public class ActionClickOnMap : ICommandMap
    {
        private readonly Action _callback;
        private readonly Action<string> _callbackLevel;
        private readonly string _level;

        public ActionClickOnMap(Action callback)
        {
            this._callback = callback;
        }

        public ActionClickOnMap(Action<string> callbackLevel, string level)
        {
            this._callbackLevel = callbackLevel;
            this._level = level;
        }

        public bool CanExecute()
        {
            return this._callback != null;
        }

        public bool CanExecuteLevel()
        {
            return this._callbackLevel != null;
        }

        public void Execute()
        {
            if (CanExecute())
                this._callback.Invoke();
            if (CanExecuteLevel())
                this._callbackLevel.Invoke(_level);
        }
    }
}