﻿using AppodealAds.Unity.Api;
using Assets.Scripts.Camera;
using Assets.Scripts.Map.Classes;
using Assets.Scripts.Sound;
using UnityEngine;

namespace Assets.Scripts.Class
{
    public class TouchGameManager : ContainerTouch, ITouchGame
    {
        [SerializeField] private GameObject EnergyShock_Left;
        [SerializeField] private GameObject EnergyShock_Right;
        [SerializeField] private UIButton button;
        private bool isActive;

        private void StartTouch()
        {

            if (!isActive /**&& ADManager.Instance.IsLoaded(Appodeal.INTERSTITIAL) **/)
                ADManager.Instance.ShowInterstitial(15f);
            isActive = !isActive;
            EnergyShock_Left.SetActive(isActive);
            EnergyShock_Right.SetActive(isActive);
            button.normalSprite = isActive ? "2" : "O-uNcp6y1VY";
            AudioManager.Instance.IsMusicOn = isActive;
            CameraManager.Instance.SetTorchEnable();
           
        }


        protected override void InitUIHandlers()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Commands.Add("ButtonStart", new ActionClickOnMap(StartTouch));
        }
    }
}