﻿using System.Collections;
using Assets.Scripts.Shared;
using UnityEngine;
using Vuforia;

namespace Assets.Scripts.Camera
{
    public class CameraManager : UnitySingleton<CameraManager>
    {
        private bool _isActive;
        private bool _isFlash = true;

        public void SetTorchEnable()
        {
            _isActive = !_isActive;
        }

        private void Start()
        {
            _isActive = false;
            _isActive = false;
        }


        private void Update()
        {
            if (!_isActive) return;
            CameraDevice.Instance.SetFlashTorchMode(_isFlash);
            Handheld.Vibrate();
            StartCoroutine(DisableFlash());
        }

        private IEnumerator DisableFlash()
        {
            yield return new WaitForSeconds(0.1f);
            _isFlash = !_isFlash;
            CameraDevice.Instance.SetFlashTorchMode(false);
        }
    }
}