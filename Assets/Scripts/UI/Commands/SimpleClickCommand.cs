﻿using System;

public class SimpleActionClickCommand : ICommandUI {

    private readonly Action _callback;

    public SimpleActionClickCommand(Action callback){
        this._callback = callback;
    }

    public bool CanExecute(){
        return this._callback != null;
    }

    public void Execute(){
        this._callback.Invoke();
    }

}