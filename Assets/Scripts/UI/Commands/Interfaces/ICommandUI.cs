﻿using System;

public interface ICommandUI {

    Boolean CanExecute();

    void Execute();

}