﻿
#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Class;
using Assets.Scripts.Extensions;
using UnityEngine;

#endregion

namespace Assets.Scripts.UI {
    /// <summary>
    ///     Base class for the root UI element.
    /// </summary>
    public abstract class UIContainerElement : MonoBehaviour {

        /// <summary>
        ///     Command dictionary for each simple UI element.
        /// </summary>
        protected Dictionary<String, ICommandUI> Commands;

        /// <summary>
        ///     Type of thee UI element.
        /// </summary>
        public UIElementType Type { get; set; }

        /// <summary>
        ///     Top UI layer value.
        /// </summary>
        public Int32 LastDepthValue { get; protected set; }

        #region Constructors

        protected UIContainerElement(){
            this.Commands = new Dictionary<string, ICommandUI>();
        }

        #endregion

        #region ##### PRIVATE METHODS #####

        /// <summary>
        ///     Subscribe all ui element to click events.
        /// </summary>
        private void Subscribe(){
            // if there's clickable element add event raiser component to it.
            foreach (var child in this.gameObject.GetComponentsInChildren<Transform>()) {
                if (child.GetComponent<BoxCollider>() != null &&
                    child.GetComponent<UIButton>() != null) {
                    child.gameObject.AddComponent<UIEventRaiser>();
                }
            }

            // sobscribe each object if it has Event Raiser component.
            foreach (var child in this.gameObject.GetComponentsInChildren<UIEventRaiser>()) {
                child.Click += this.EventWorker;
            }
        }

        #endregion

        #region ##### PROTECTED METHODS #####

        /// <summary>
        ///     Invokes the click handler from Actions dictionary.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EventWorker(object sender, UIEventArgs e){
            try {

                this.Commands[e.Info].Execute();
            }
            catch (KeyNotFoundException) {
#if UNITY_EDITOR
                Debug.LogWarning("There is no click handler in " + this.Type + " e.info is " + e.Info +
                                 "No command was executed");
#endif
            }
        }

        protected virtual void CalculateLastDepthValue(){
           
                var maxSpriteDepthValue = this.gameObject.GetComponentsInChildren<UISprite>().Max(m => m.depth);
                var maxLabelDepthValue = this.gameObject.GetComponentsInChildren<UILabel>().Max(m => m.depth);
                this.LastDepthValue = maxLabelDepthValue > maxSpriteDepthValue
                    ? maxLabelDepthValue
                    : maxSpriteDepthValue;
           
        }

        /// <summary>
        ///     Additional init method for init needed singletones,
        ///     or if need some init before subscribe.
        /// </summary>
        protected virtual void PreInit(){
        }

        /// <summary>
        ///     Inits all click handlers.
        /// </summary>
        protected abstract void InitUIHandlers();

        /// <summary>
        ///     Fills child UI elements with some information.
        /// </summary>
        protected abstract void FillUIElementsInfo();

        #endregion

        #region ##### UNITY REFLECTION CALLBACKS #####

        /// <summary>
        ///     Unity's awake reflection method.
        /// </summary>
        protected virtual void Awake(){
            this.PreInit();
            this.InitUIHandlers();
        }

        /// <summary>
        ///     Unity's start reflection method.
        /// </summary>
        protected virtual void Start(){
            this.Subscribe();
            this.CalculateLastDepthValue();
            this.FillUIElementsInfo();
        }

        protected virtual void Update() {
        }

        /// <summary>
        ///     Unity destroy callback.
        /// </summary>
        protected virtual void OnDestroy(){
            this.Commands = null;

            foreach (var child in this.gameObject.GetComponentsInChildren<UIEventRaiser>()) {
                child.Click -= this.EventWorker;
            }
        }

        #endregion

        #region ##### PUBLIC METHODS #####

        /// <summary>
        ///     Enables all children colliders.
        /// </summary>
        public virtual void EnableInput(){
#if UNITY_EDITOR
            Debug.Log("Enabling input in " + this.Type);
#endif
            this.gameObject.SetColliderStateInAllChildren(true);
        }

        /// <summary>
        ///     Disabled all children colliders.
        /// </summary>
        public virtual void DisableInput(){
#if UNITY_EDITOR

            Debug.Log("Disabling input in " + this.Type);
            this.gameObject.SetColliderStateInAllChildren(false);
#endif
        }

        #endregion
    }

}