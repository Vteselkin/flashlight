﻿#region Usings

using System;
using Assets.Scripts.GUI.Managers;
using Assets.Scripts.UI;
using UnityEngine;

#endregion

namespace Assets.Scripts.GUI.Popups {
    internal abstract class PopupBase : UIContainerElement {

        public UIButton BtnClose;
        public Action OnClosePopup;

        public UIContainerElement Parent { get; protected set; }

        public void Init(UIContainerElement parent){
            this.Parent = parent;
        }


        /// <summary>
        ///     Inits all click handlers.
        /// </summary>
        protected override void InitUIHandlers(){
            if (BtnClose != null) {
                this.Commands.Add(BtnClose.name, new SimpleActionClickCommand(this.Close));   
            }
        }

        protected virtual void Close(){
            if (OnClosePopup != null)
                OnClosePopup();
            UIManager.Instance.RemoveElement(this);
        }

        protected virtual void CloseWithoutEventRaising(){
            UIManager.Instance.RemoveElement(this);
        }
    }
}