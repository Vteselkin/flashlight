﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Class;

namespace Assets.Scripts.UI {
    internal static class UiConstants {
        public const string ElementsPrefabsPath = "Prefabs/UI/";

        public static Dictionary<UIElementType, String> UiPrefabNames;

        static UiConstants() {
            UiPrefabNames = new Dictionary<UIElementType, string>() {
                {UIElementType.DarkAlpha, "Popups/DarkAlpha"}

               
            };
        }
    }
}