﻿

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Class;
using Assets.Scripts.GUI.Popups;
using Assets.Scripts.UI;
using UnityEngine;
using Object = UnityEngine.Object;

#endregion

namespace Assets.Scripts.GUI.Managers {

    /// <summary>
    /// Main UI management class.
    /// </summary>
    internal class UIManager {
        #region ##### LOCAL CONSTANTS #####
        private const Int32 MinUiElementsDepthDiff = 2;

        #endregion

        #region ##### PRIVATE FIELDS #####

        private readonly List<UIContainerElement> _uiParentElements;

        // dark background  UI to hide lower depth UI elements.
        private GameObject _darkAlphaObj;

        private readonly List<UIElementType> _uniqueElemetTypes;

        #endregion

        #region ##### SINGLETON MEMBERS ####

        private static readonly UIManager instance = new UIManager();

        #region Constructors
        static UIManager()
        {
        }

        private UIManager()
        {
            this._uiParentElements = new List<UIContainerElement>();
            _uniqueElemetTypes = new List<UIElementType>();
            InitUniqueElementTypes();
        } 
        #endregion

        public static UIManager Instance{
            get { return instance; }
        }

        #endregion

        #region ##### PRIVATE METHODS #####

        private void InitUniqueElementTypes(){ 
            
        }

        private GameObject InstantiateUiElement(UIElementType type){
            try{
                var elementNewGameObject =
                    Object.Instantiate(Resources.Load(UiConstants.ElementsPrefabsPath
                                                        + UiConstants.UiPrefabNames[type])) as GameObject;
                
                return elementNewGameObject;
            }

            catch (KeyNotFoundException) {
#if UNITY_EDITOR
                Debug.LogError("There is no prefab for " + type + " in dictionary");
#endif
                return null;
            }

        }
        
        /// <summary>
        /// Normalizes new popup's child elements depth.
        /// </summary>
        /// <param name="prevElementMaxDepthValue">Prev layer depth value.</param>
        /// <param name="newElementGameObj">New created game object.</param>
        private static void NormalizeNewСreatedPopupDepth(Int32 prevElementMaxDepthValue, GameObject newElementGameObj){

            IncreaseDepthInChildrenByType<UISprite>(newElementGameObj, prevElementMaxDepthValue);
            IncreaseDepthInChildrenByType<UILabel>(newElementGameObj, prevElementMaxDepthValue);

        }

        private static void IncreaseDepthInChildrenByType<T>(GameObject gameObj, Int32 prevElementMaxDepthValue)
            where T : UIWidget{
            foreach (var obj in gameObj.GetComponentsInChildren<T>()) {
                obj.depth = prevElementMaxDepthValue + MinUiElementsDepthDiff + obj.depth;
            }
        }

        /// <summary>
        ///     Normalizes dark alpha UI element depth.
        /// </summary>
        /// <param name="depthTopLayerObj">Top layer depth.</param>
        /// <param name="depthPrevLayerObj">Bottom layer depth.</param>
        private void NormalizeDarkAlphaObjectDepth(Int32 depthTopLayerObj,
                                                   Int32 depthPrevLayerObj){

            this._darkAlphaObj.gameObject.GetComponentInChildren<UISprite>().depth
                = (depthTopLayerObj + depthPrevLayerObj)/
                  2;
        }

        #endregion

        #region ##### PUBLIC METHODS #####

        /// <summary>
        ///     Instantiales UI element by type.
        /// </summary>
        /// <typeparam name="T">Type of needed object to create.</typeparam>
        /// <param name="type">UI element type.</param>
        /// <param name="parent">Parent element if exists.</param>
        /// <returns>Created UI object.</returns>
        public T CreateNewUIElement<T>(UIElementType type, bool addDarkBackground = true, UIContainerElement parent = null)
            where T : UIContainerElement
        {
         
            if (_uniqueElemetTypes.Contains(type)){
                foreach (var element in _uiParentElements){
                    if (element.Type == type){
                        return element as T;
                    }
                }
            }

            var elementNewGameObject = this.InstantiateUiElement(type);

            if (elementNewGameObject != null) {

                var scale = elementNewGameObject.transform.localScale;

                elementNewGameObject.transform.parent = Object.FindObjectOfType<UIRoot>().transform;

                elementNewGameObject.transform.localScale = scale;

                var elementNew = elementNewGameObject.GetComponent<UIContainerElement>();

                if (elementNew is PopupBase){
                    _uiParentElements.Add(elementNew);
                    //isttantiate dark alpha UI element if we need it.
                    if (this._darkAlphaObj == null && addDarkBackground){
                        this._darkAlphaObj = this.InstantiateUiElement(UIElementType.DarkAlpha);
                        this._darkAlphaObj.transform.parent = Object.FindObjectOfType<UIRoot>().transform;
                    }
                }
                elementNew.Type = type;
            }

            return this._uiParentElements.LastOrDefault() as T;
        }

        /// <summary>
        /// Add new UI element.
        /// </summary>
        /// <typeparam name="T">Type of needed object to create.</typeparam>
        /// <param name="type">UI element type.</param>
        /// <param name="elementNewGameObject">Element for adding</param>
        /// <param name="parent">Parent element if exists.</param>
        /// <returns>Created UI object.</returns>
        public T AddNewUIElement<T>(UIElementType type, UIContainerElement elementNewGameObject, bool addDarkBackground = true, UIContainerElement parent = null)
            where T : UIContainerElement{

            if (_uniqueElemetTypes.Contains(type)){
                foreach (var element in _uiParentElements){
                    if (element.Type == type){
                        return element as T;
                    }
                }
            }

            if (elementNewGameObject != null){

                var scale = elementNewGameObject.transform.localScale;

                elementNewGameObject.transform.parent = Object.FindObjectOfType<UIRoot>().transform;

                elementNewGameObject.transform.localScale = scale;

                var elementNew = elementNewGameObject.GetComponent<UIContainerElement>();

               
                elementNew.Type = type;
            }

            return this._uiParentElements.LastOrDefault() as T;
        }

        /// <summary>
        ///     Gets needed element by generic type.
        /// </summary>
        /// <typeparam name="T">Needed UI element type.</typeparam>
        /// <returns>The UI element.</returns>
        public T GetUiElement<T>() where T : UIContainerElement{

            return this._uiParentElements.FirstOrDefault(f => f is T) as T;
        }

        /// <summary>
        ///     Removes passed UI element from game.
        /// </summary>
        /// <param name="element">UI element to remove.</param>
        public void RemoveElement(UIContainerElement element){

            var popup = element as PopupBase;

            if (popup == null){
                return;
            }

            _uiParentElements.Remove(popup);
            NGUITools.DestroyImmediate(element.gameObject);
            
                // if dark exists
            if (_uiParentElements.Count == 0 && this._darkAlphaObj != null){
                NGUITools.DestroyImmediate(this._darkAlphaObj);
                this._darkAlphaObj = null;
            }
        }

        /// <summary>
        ///     Clears ui collection.
        /// </summary>
        public void ClearUIElementCollection(){
            this._uiParentElements.Clear();
        }

        public int GetTopPopup()
        {
            return _uiParentElements.Count;
            
        }

       

        #endregion
    }
}