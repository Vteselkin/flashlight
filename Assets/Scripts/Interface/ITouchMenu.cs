﻿namespace Assets.Scripts.Interface
{
    public interface ITouchMenu{

        void StartGame();
        void Plus();
        void Minus();
        void GetMore();
        void GetPro();
        void NoAds();
    }
}
