﻿using UnityEngine;

namespace Assets.Scripts.Map.Interface
{
    public interface ISubscribe
    {
        void Init();
        void Subscribe();
        void Unsubscribe();
    }
}