﻿using UnityEngine;
using System.Collections;

public interface ITouchInput
{
    void TextBox();
    void InputButton();
}
