﻿using UnityEngine;
using System.Collections;

public interface ICommandMap
{

    bool CanExecute();
    bool CanExecuteLevel();
    void Execute();


}
