﻿using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using Assets.Scripts.Shared;
using UnityEngine;

public class ADManager : UnitySingleton<ADManager>, IBannerAdListener
{
    private const string appKey = "5c7399265d54857fec9bb2cf07c5c319fa0a76acda36f9cd";
    private bool waitAD = false;
    // Use this for initialization
    public override void Init()
    {
        Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER);
        Debug.Log("Appodeal.initialize");
    }
    public void Show(int index)
    {
        Appodeal.show(index);
    }
    public void ShowInterstitial(float second)
    {
        if (!waitAD)
        {
            waitAD = true;
            StartCoroutine(ShowInterstitialWithDelay(second));
        }
    }

    public bool IsLoaded(int index)
    {
        return Appodeal.isLoaded(index);
    }
    private IEnumerator ShowInterstitialWithDelay(float second)
    {
        yield return new WaitForSeconds(second);
        waitAD = false;
        Appodeal.show(Appodeal.INTERSTITIAL);
    }

    public void onBannerLoaded()
    {
        Debug.LogWarning("onBannerLoaded");
    }

    public void onBannerFailedToLoad()
    {
        Debug.LogError("onBannerFailedToLoad");
    }

    public void onBannerShown()
    {
        Debug.LogWarning("onBannerShown");
    }

    public void onBannerClicked()
    {
        Debug.LogWarning("onBannerClicked");
    }
}