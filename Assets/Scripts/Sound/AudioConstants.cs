﻿using System.Collections.Generic;


public enum UIAudioClipTypes{
    Button = 0,
    Spalsh = 1,
    Game = 2,
    
   
}

namespace Assets.Scripts.Sound{
    internal static class SoundConstants{

        #region Const

        public const string MusicPath = "Sounds/";
        public static List<string> ListSoundPathTrackSelect;
        public static List<string> ListSoundPathGemDamage;
      

        public static Dictionary<UIAudioClipTypes, string> ListSoundUi;

      

        #endregion

        #region Constructors

        static SoundConstants(){
           InitSoundUi();
           
        }

      
        static void InitSoundUi(){
            ListSoundUi = new Dictionary<UIAudioClipTypes, string>();
            ListSoundUi[UIAudioClipTypes.Button] = "Botton";
            ListSoundUi[UIAudioClipTypes.Spalsh] = "Splash";
            ListSoundUi[UIAudioClipTypes.Game] = "Gameplay";
          
           
        }

        #endregion
    }
}
