﻿using System.Collections.Generic;
using Assets.Scripts.Shared;
using UnityEngine;

namespace Assets.Scripts.Sound{
    public class AudioManager : UnitySingleton<AudioManager>{
        #region Fields

        private AudioSource _soundAudioSource;
        private AudioSource _musicAudioSource;


        private bool _isMusicOn;
        private bool _isSoundOn;

        #region Sounds

        private List<AudioClip> _listSoundTrackSelect;
        private List<AudioClip> _listSoundGemDamage;


        private Dictionary<UIAudioClipTypes, AudioClip> _listSoundUi;


        #endregion

        #endregion

        #region Properties

        public float SoundClipLength{
            get{
                if (_soundAudioSource.clip != null){
                    return _soundAudioSource.clip.length;
                }
                return 0;
            }
        }

        public float MusicClipLength{
            get{
                if (_musicAudioSource.clip != null){
                    return _musicAudioSource.clip.length;
                }
                return 0;
            }
        }
        
        public bool IsMusicOn{
            get{
                return _isMusicOn;
            }
            set{
                if (_isMusicOn == value){
                    return;
                }

                _isMusicOn = value;

                if (_isMusicOn){
                  
                }
                else{
                    _musicAudioSource.Stop();
                }
            }
        }
        
        public bool IsSoundOn{
            get{
                return _isSoundOn;
            }
            set{
                if (_isSoundOn == value)
                    return;

                _isSoundOn = value;

                if (_isSoundOn)
                    NGUITools.soundVolume = 1.0f;
                else
                    NGUITools.soundVolume = 0.0f;
            }
        }

        #endregion

        #region Constructors

        public void Initialize(){
            _soundAudioSource = gameObject.AddComponent<AudioSource>();
            _musicAudioSource = gameObject.AddComponent<AudioSource>();

          InitAudioClips();



        
        }

        private void InitAudioClips(){
            InitAudioClips(out _listSoundUi, SoundConstants.ListSoundUi);
          
        }


        private void InitAudioClips<T>(out Dictionary<T, AudioClip> dictionaryClip, Dictionary<T, string> dictionaryPath){
            dictionaryClip = new Dictionary<T, AudioClip>();
            foreach (var clipPath in dictionaryPath){
                dictionaryClip.Add(clipPath.Key, Resources.Load<AudioClip>(SoundConstants.MusicPath + clipPath.Value));
            }
        }

        

      

        #endregion

        #region Public methods

        public void StopPlayingSound(){
            _soundAudioSource.Stop();
        }

     

        public void PlaySoundUi(UIAudioClipTypes type, object specialParam = null){
       
                    PlayUiAudioClip(type);
  
        }

      

        public void PlaySoundRemoveFromTrack(int objectsInTrack){
            if (objectsInTrack >= _listSoundTrackSelect.Count){
                objectsInTrack = _listSoundTrackSelect.Count - 1;
            }
            PlaySoundOneShot(_listSoundTrackSelect[objectsInTrack - 1]);
        }

        #endregion

        #region Private methods

       private void PlaySoundOneShot(AudioClip clip){
            if (clip != null){
                _soundAudioSource.PlayOneShot(clip);
            }
        }

       

        private void PlayUiAudioClip(UIAudioClipTypes type){
            PlaySoundOneShot(_listSoundUi[type]);
        }


        private void Update()
        {
            if (IsMusicOn && !_musicAudioSource.isPlaying )
            {
                _musicAudioSource.clip = _listSoundUi[UIAudioClipTypes.Game];
                _musicAudioSource.Play();
            }
        }
      

        #endregion  

       

       
    }
}
